-- Franchises
INSERT INTO franchise ("name", "description") VALUES (
                                                      'James Bond',
                                                      'The James Bond franchise is a long-running series of spy fiction stories that originated with the novels of British author Ian Fleming and became a pop culture phenomenon with a series of films that started in 19621. The franchise features a British secret agent working for MI6 under the codename 007, who has been portrayed by different actors over time2. The franchise is known for its action, gadgets, cars, villains and romance.'
                                                        );
-- Movies
INSERT INTO movie ("title", "genre", "year", "director", "picture_url", "trailer_url", "franchise_id") VALUES (
                                                                                       'Dr. No', 'Action, Adventure, Thriller', 1962, 'Terence Young',
                                                                                       'https://www.imdb.com/title/tt0055928/mediaviewer/rm644487169/?ref_=tt_ov_i',
                                                                                       'https://www.imdb.com/video/vi2536031001/?playlistId=tt0055928&ref_=tt_pr_ov_vi', 1
                                                                                      );
INSERT INTO movie ("title", "genre", "year", "director", "picture_url", "trailer_url", "franchise_id") VALUES (
                                                                                          'Live and Let Die', 'Action, Adventure, Thriller', 1973, 'Guy Hamilton',
                                                                                          'https://www.imdb.com/title/tt0070328/?ref_=nv_sr_srsg_0',
                                                                                          'https://www.youtube.com/watch?v=KTzsm9-XWQo', 1
                                                                                      );
INSERT INTO movie ("title", "genre", "year", "director", "picture_url", "trailer_url", "franchise_id") VALUES (
                                                                                          'GoldenEye', 'Action, Adventure, Thriller', 1995, 'Martin Campbell',
                                                                                          'https://www.imdb.com/title/tt0113189/mediaviewer/rm486027265/?ref_=tt_ov_i',
                                                                                          'https://www.imdb.com/video/vi2674918169/?playlistId=tt0113189&ref_=tt_ov_vi', 1
                                                                                      );

-- Characters
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('James Bond', '007', 'Male', 'https://www.imdb.com/title/tt0055928/mediaviewer/rm1757837056?ref_=ttmi_mi_all_sf_90');
INSERT INTO character ("name", "gender", "picture_url") VALUES ('Honey Ryder', 'Female', 'https://www.imdb.com/title/tt0055928/mediaviewer/rm2516532737?ref_=ttmi_mi_all_sf_1');
INSERT INTO character ("name", "picture_url") VALUES ('M', 'https://www.imdb.com/title/tt0055928/mediaviewer/rm35862272?ft0=name&fv0=nm0496866&ft1=image_type&fv1=still_frame&ref_=tt_ch');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Dr. Julius no', 'Dr. No', 'Male', 'https://www.imdb.com/title/tt0055928/mediaviewer/rm110986496?ft0=name&fv0=nm0936476&ft1=image_type&fv1=still_frame&ref_=tt_ch');
INSERT INTO character ("name", "gender", "picture_url") VALUES ('Miss Moneypenny', 'Female', 'https://www.imdb.com/title/tt0059800/mediaviewer/rm3634006273?ft0=name&fv0=nm0561755&ft1=image_type&fv1=still_frame&ref_=tt_ch');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Dr. Kananga', 'Mr. Big', 'Male', 'https://www.imdb.com/title/tt0070328/mediaviewer/rm2711521537?ft0=name&fv0=nm0001433&ft1=image_type&fv1=still_frame&ref_=tt_ch');
INSERT INTO character ("name", "gender", "picture_url") VALUES ('Solitaire', 'Female', 'https://www.imdb.com/title/tt0070328/mediaviewer/rm1233956352?ft0=name&fv0=nm0005412&ft1=image_type&fv1=still_frame&ref_=tt_ch');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Alec Trevelyan', '006', 'Male', 'https://www.imdb.com/title/tt0113189/mediaviewer/rm2204469761?ft0=name&fv0=nm0000293&ft1=image_type&fv1=still_frame&ref_=tt_ch');
INSERT INTO character ("name", "gender", "picture_url") VALUES ('Natalya Simonova', 'Female', 'https://www.imdb.com/title/tt0113189/mediaviewer/rm3619153921?ft0=name&fv0=nm0001713&ft1=image_type&fv1=still_frame&ref_=tt_ch');
INSERT INTO character ("name", "gender", "picture_url") VALUES ('Xenia Onetopp', 'Female', 'https://www.imdb.com/title/tt0113189/mediaviewer/rm1684834561?ft0=name&fv0=nm0000463&ft1=image_type&fv1=still_frame&ref_=tt_ch');

-- Movie_Character_Relationship
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('1', '1');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('1', '2');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('1', '3');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('1', '4');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('1', '5');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('2', '1');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('2', '3');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('2', '5');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('2', '6');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('2', '7');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('3', '1');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('3', '3');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('3', '5');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('3', '8');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('3', '9');
INSERT INTO movie_character ("movie_id", "character_id") VALUES ('3', '10');