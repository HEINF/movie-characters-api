package dk.experis.movie.controllers;

import dk.experis.movie.mappers.CharacterMapper;
import dk.experis.movie.mappers.MovieMapper;
import dk.experis.movie.models.Movie;
import dk.experis.movie.models.dtos.character.CharacterDTO;
import dk.experis.movie.models.dtos.movie.CreateMovieDTO;
import dk.experis.movie.models.dtos.movie.MovieDTO;
import dk.experis.movie.models.dtos.movie.UpdateMovieDTO;
import dk.experis.movie.services.CharacterService;
import dk.experis.movie.services.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper, CharacterService characterService, CharacterMapper characterMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Delete a movie by its ID")
    @ApiResponse(responseCode = "204", description = "No Content")
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get a movie by ID")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = MovieDTO.class))})
    @GetMapping("{id}")
    public ResponseEntity getById(@PathVariable int id) {
        MovieDTO movieDTO = movieMapper.movieToMovieDTO(movieService.findById(id));
        return ResponseEntity.ok(movieDTO);
    }

    @Operation(summary = "Get all movies")
    @ApiResponse(responseCode = "200",
                description = "Success",
                content = {@Content(mediaType = "application/json",
                schema = @Schema(implementation = MovieDTO.class))})
    @GetMapping
    public ResponseEntity getAll() {
        Collection<MovieDTO> movieDTOs = movieMapper.movieToMovieDTO(movieService.findAll());
        return ResponseEntity.ok(movieDTOs);
    }

    @Operation(summary = "Get characters in a movie")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = CharacterDTO.class))})
    @GetMapping("{id}/characters")
    public ResponseEntity getCharactersInMovie(@PathVariable int id) {
        MovieDTO movieDTO = movieMapper.movieToMovieDTO(movieService.findById(id));
        List<CharacterDTO> characterDTOs = new ArrayList<>();
        for(Integer charId: movieDTO.getCharacters() ) {
            characterDTOs.add(characterMapper.characterToCharacterDTO(characterService.findById(charId)));
        }
        return ResponseEntity.ok(characterDTOs);
    }

    @Operation(summary = "Add characters to a movie by character IDs")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = MovieDTO.class))})
    @PatchMapping("{movieId}/characters")
    public ResponseEntity updateCharacters(@RequestBody Integer[] charIds, @PathVariable int movieId) {
        MovieDTO movieDTO = movieMapper.movieToMovieDTO(movieService.findById(movieId));
        for (Integer charId: charIds) {
            if(!movieDTO.getCharacters().contains(charId)) {
                movieDTO.getCharacters().add(charId);
            }
        }
        movieService.update(movieMapper.movieDtoToMovie(movieDTO));
        return ResponseEntity.ok(movieDTO);
    }

    @Operation(summary = "Update a movie")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = MovieDTO.class))})
    @PatchMapping("{id}")
    public ResponseEntity update(@RequestBody UpdateMovieDTO movieDTO, @PathVariable int id) {
        Movie movie = movieService.findById(id);
        movieService.update(movieMapper.updateMovieDtoToMovie(movieDTO, movie));
        return ResponseEntity.ok(movieMapper.movieToMovieDTO(movie));
    }

    @Operation(summary = "Add a movie")
    @ApiResponse(responseCode = "201",
            description = "Created",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = MovieDTO.class))})
    @PostMapping
    public ResponseEntity add(@RequestBody CreateMovieDTO movieDTO) {
        Movie movie = movieService.add(movieMapper.createMovieDtoToMovie(movieDTO));
        URI location = URI.create("movies/" + movie.getId());
        return ResponseEntity.created(location).body(movieMapper.movieToMovieDTO(movie));
    }
}
