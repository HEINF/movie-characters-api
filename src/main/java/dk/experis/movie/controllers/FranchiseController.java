package dk.experis.movie.controllers;

import dk.experis.movie.mappers.CharacterMapper;
import dk.experis.movie.mappers.FranchiseMapper;
import dk.experis.movie.mappers.MovieMapper;
import dk.experis.movie.models.Franchise;
import dk.experis.movie.models.dtos.character.CharacterDTO;
import dk.experis.movie.models.dtos.franchise.CreateFranchiseDTO;
import dk.experis.movie.models.dtos.franchise.UpdateFranchiseDTO;
import dk.experis.movie.models.dtos.franchise.FranchiseDTO;
import dk.experis.movie.models.dtos.movie.MovieDTO;
import dk.experis.movie.services.CharacterService;
import dk.experis.movie.services.FranchiseService;
import dk.experis.movie.services.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public FranchiseController
            (
            FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieService movieService,
            MovieMapper movieMapper, CharacterService characterService, CharacterMapper characterMapper
            )
            {
                this.franchiseService = franchiseService;
                this.franchiseMapper = franchiseMapper;
                this.movieService = movieService;
                this.movieMapper = movieMapper;
                this.characterService = characterService;
                this.characterMapper = characterMapper;
            }

    @Operation(summary = "Delete a franchise by its ID")
    @ApiResponse(responseCode = "204", description = "No Content")
    @DeleteMapping("{id}")
    public  ResponseEntity delete(@PathVariable int id) {
        // Remove franchise from Movies (Relation owner) before deleting to avoid FK constraint error
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDTO(franchiseService.findById(id));
        for(Integer movieId: franchiseDTO.getMovies()){
            movieService.removeFranchise(movieId);
        }
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
    @Operation(summary = "Get a franchise by its ID")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = FranchiseDTO.class))})
    @GetMapping({"{id}"})
    public ResponseEntity findById(@PathVariable int id) {
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDTO(franchiseService.findById(id));
        return ResponseEntity.ok(franchiseDTO);
    }

    @Operation(summary = "Get all franchises")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = FranchiseDTO.class))})
    @GetMapping
    public  ResponseEntity findAll() {
        Collection<FranchiseDTO> franchiseDTOs = franchiseMapper.franchiseToFranchiseDTO(franchiseService.findAll());
        return ResponseEntity.ok(franchiseDTOs);
    }

    @Operation(summary = "Get all movies in a franchise")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = MovieDTO.class))})
    @GetMapping("{id}/movies")
    public ResponseEntity getMoviesInFranchise(@PathVariable int id) {
        List<MovieDTO> movieDTOs = new ArrayList<>();
        FranchiseDTO franchiseDto = franchiseMapper.franchiseToFranchiseDTO(franchiseService.findById(id));
        for(Integer movieId: franchiseDto.getMovies()) {
            movieDTOs.add(movieMapper.movieToMovieDTO(movieService.findById(movieId)));
        }
        return ResponseEntity.ok(movieDTOs);
    }

    @Operation(summary = "Get all characters in a franchise")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = CharacterDTO.class))})
    @GetMapping("{id}/characters")
    public ResponseEntity getCharactersInFranchise(@PathVariable int id) {
        List<CharacterDTO> characterDTOs = new ArrayList<>();
        List<Integer> characterIds = new ArrayList<>();
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDTO(franchiseService.findById(id));
        for(Integer movieId: franchiseDTO.getMovies()) {
            MovieDTO movieDTO = movieMapper.movieToMovieDTO(movieService.findById(movieId));
            for(Integer charId: movieDTO.getCharacters()){
                if (!characterIds.contains(charId)){
                    characterIds.add(charId);
                }
            }
        }
        for(Integer charId: characterIds){
            characterDTOs.add(characterMapper.characterToCharacterDTO(characterService.findById(charId)));
        }
        return ResponseEntity.ok(characterDTOs);
    }

    @Operation(summary = "Update a franchise")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = FranchiseDTO.class))})
    @PatchMapping("{id}")
    public ResponseEntity update(@RequestBody UpdateFranchiseDTO franchiseDTO, @PathVariable int id) {
        Franchise franchise = franchiseService.findById(id);
        franchiseService.update(franchiseMapper.updateFranchiseDtoToFranchise(franchiseDTO, franchise));
        return ResponseEntity.ok(franchiseMapper.franchiseToFranchiseDTO(franchise));
    }

    @Operation(summary = "Add movies to franchise by movie IDs")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = FranchiseDTO.class))})
    @PatchMapping("{franchiseId}/movies")
    public ResponseEntity updateCharacters(@RequestBody Collection<Integer> movieIds, @PathVariable int franchiseId) {
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDTO(franchiseService.findById(franchiseId));
        for (Integer movieId: movieIds) {
            if (!franchiseDTO.getMovies().contains(movieId)){
                franchiseDTO.getMovies().add(movieId);
                movieService.addFranchise(movieId, franchiseMapper.franchiseDtoToFranchise(franchiseDTO));
            }
        }
        franchiseService.update(franchiseMapper.franchiseDtoToFranchise(franchiseDTO));
        return ResponseEntity.ok(franchiseDTO);
    }

    @Operation(summary = "Add a franchise")
    @ApiResponse(responseCode = "201",
            description = "Created",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = FranchiseDTO.class))})
    @PostMapping
    public ResponseEntity add(@RequestBody CreateFranchiseDTO franchiseDTO) {
        Franchise franchise = franchiseService.add(franchiseMapper.createFranchiseDtoToFranchise(franchiseDTO));
        URI location = URI.create("franchises/" + franchise.getId());
        return ResponseEntity.status(HttpStatus.CREATED).body(franchiseMapper.franchiseToFranchiseDTO(franchise));
    }
}
