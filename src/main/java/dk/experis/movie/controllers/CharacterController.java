package dk.experis.movie.controllers;

import dk.experis.movie.mappers.CharacterMapper;
import dk.experis.movie.mappers.MovieMapper;
import dk.experis.movie.models.Character;
import dk.experis.movie.models.dtos.character.CharacterDTO;
import dk.experis.movie.models.dtos.character.CreateCharacterDTO;
import dk.experis.movie.models.dtos.character.UpdateCharacterDTO;
import dk.experis.movie.services.CharacterService;
import dk.experis.movie.services.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {

    private final CharacterService characterService;
    private final CharacterMapper characterMapper;
    private final MovieService movieService;
    private final MovieMapper movieMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper, MovieService movieService, MovieMapper movieMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    @Operation(summary = "Delete a character by its ID")
    @ApiResponse(responseCode = "204", description = "No Content")
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        // Remove character from linking table through MovieService (Movie is owner) to avoid FK constraint issue on deletion
        CharacterDTO characterDTO = characterMapper.characterToCharacterDTO(characterService.findById(id));
        Set<Integer> moviesToRemoveFrom = characterDTO.getMovies();
        for(Integer movieId: moviesToRemoveFrom){
            movieService.removeCharacter(movieId, id);
        }
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get all characters")
    @ApiResponse(responseCode = "200",
                description = "Success",
                content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CharacterDTO.class))})
    @GetMapping
    public ResponseEntity getAll() {
        Collection<CharacterDTO> characterDTOs = characterMapper.characterToCharacterDTO(characterService.findAll());
        return ResponseEntity.ok(characterDTOs);
    }

    @Operation(summary = "Get a character by its ID")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CharacterDTO.class))})
    @GetMapping("{id}")
    public ResponseEntity getById(@PathVariable int id) {
        CharacterDTO characterDTO = characterMapper.characterToCharacterDTO(characterService.findById(id));
        return ResponseEntity.ok(characterDTO);
    }

    @Operation(summary = "Update a character")
    @ApiResponse(responseCode = "200",
                description = "Success",
                content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CharacterDTO.class))})
    @PatchMapping("{id}")
    public ResponseEntity update(@RequestBody UpdateCharacterDTO characterDTO, @PathVariable int id) {
        Character character = characterService.findById(id);
        characterService.update(characterMapper.updateCharacterDtoToCharacter(characterDTO, character));
        return ResponseEntity.ok(characterMapper.characterToCharacterDTO(character));
    }

    @Operation(summary = "Add a character")
    @ApiResponse(responseCode = "201",
            description = "Created",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CharacterDTO.class))})
    @PostMapping
    public ResponseEntity add(@RequestBody CreateCharacterDTO characterDTO) {
        Character character = characterService.add(characterMapper.createCharacterDtoToCharacter(characterDTO));
        URI location = URI.create("characters/" + character.getId());
        return ResponseEntity.created(location).body(characterMapper.characterToCharacterDTO(character));
    }

}
