package dk.experis.movie.services;

import dk.experis.movie.models.Character;

public interface CharacterService extends CrudService <Character, Integer>{

}
