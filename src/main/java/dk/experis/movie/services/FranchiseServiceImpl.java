package dk.experis.movie.services;

import dk.experis.movie.models.Franchise;
import dk.experis.movie.repositories.FranchiseRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class FranchiseServiceImpl implements FranchiseService{
    private final FranchiseRepository franchiseRepository;


    public FranchiseServiceImpl (FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    /**
     * Find a Franchise by id.
     * @param id used to find Franchise Object.
     * @return Franchise Object with given id. Can be null.
     */
    @Override
    public Franchise findById(Integer id) {

        return this.franchiseRepository.findById(id).get();
    }

    /**
     * Finds all Franchise in the database.
     * @return Collection of Franchise Objects. Can be empty.
     */
    @Override
    public Collection<Franchise> findAll() {

        return this.franchiseRepository.findAll();
    }

    /**
     * Adds a Franchise Object to the database.
     * @param entity of type Franchise.
     * @return Franchise Object that has been created.
     */
    @Override
    public Franchise add(Franchise entity) {

        return franchiseRepository.save(entity);
    }

    /**
     * Updates a Franchise Object.
     * @param entity of type Franchise.
     * @return Updated Franchise Object.
     */
    @Override
    public Franchise update(Franchise entity) {

        return franchiseRepository.save(entity);
    }

    /**
     * Deletes a Franchise Object by its id.
     * @param id of the Franchise Object to be deleted.
     */
    @Override
    public void deleteById(Integer id) {
        franchiseRepository.deleteById(id);
    }

    /**
     * Delete a Franchise Object.
     * @param entity of Type Franchise.
     */
    @Override
    public void delete(Franchise entity) {

    }

}
