package dk.experis.movie.services;

import dk.experis.movie.models.Franchise;

public interface FranchiseService extends CrudService<Franchise, Integer> {
}
