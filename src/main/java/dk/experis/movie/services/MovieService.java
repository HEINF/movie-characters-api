package dk.experis.movie.services;

import dk.experis.movie.models.Franchise;
import dk.experis.movie.models.Movie;

import java.util.Collection;

public interface MovieService extends CrudService <Movie, Integer> {
    void addFranchise(Integer movieId, Franchise franchises);

    void removeFranchise(Integer movieId);

    void removeCharacter(Integer movieId, Integer characterId);
}
