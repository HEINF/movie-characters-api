package dk.experis.movie.services;

import dk.experis.movie.models.Character;
import dk.experis.movie.repositories.CharacterRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CharacterServiceImpl implements CharacterService{

    private final CharacterRepository characterRepository;
    public CharacterServiceImpl (CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;

    }

    /**
     * Find a character by id.
     * @param id used to find Character Object.
     * @return Character Object with given id. Can be null.
     */
    @Override
    public Character findById(Integer id) {

        return characterRepository.findById(id).get();
    }

    /**
     * Finds all Characters in the database.
     * @return Collection of Character Objects. Can be empty.
     */
    @Override
    public Collection<Character> findAll() {

        return characterRepository.findAll();
    }

    /**
     * Adds a Character Object to the database.
     * @param entity of type Character.
     * @return Character Object that has been created.
     */
    @Override
    public Character add(Character entity) {

        return characterRepository.save(entity);
    }

    /**
     * Updates a Character Object.
     * @param entity of type Character.
     * @return Updated Character Object.
     */
    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    /**
     * Deletes a Character Object by its id.
     * @param id of the Character Object to be deleted.
     */
    @Override
    public void deleteById(Integer id) {
        characterRepository.deleteById(id);
    }

    /**
     * Delete a Character Object.
     * @param entity of Type Character.
     */
    @Override
    public void delete(Character entity) {

    }
}
