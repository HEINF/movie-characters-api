package dk.experis.movie.services;

import dk.experis.movie.models.Character;
import dk.experis.movie.models.Franchise;
import dk.experis.movie.models.Movie;
import dk.experis.movie.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MovieServiceImpl implements MovieService{
    private final MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }


    /**
     * Find a Movie by id.
     * @param id used to find Movie Object.
     * @return Movie Object with given id. Can be null.
     */
    @Override
    public Movie findById(Integer id) {
        return this.movieRepository.findById(id).get();
    }

    /**
     * Finds all Movie in the database.
     * @return Collection of Movie Objects. Can be empty.
     */
    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    /**
     * Adds a Movie Object to the database.
     * @param entity of type Movie.
     * @return Movie Object that has been created.
     */
    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    /**
     * Updates a Movie Object.
     * @param entity of type Movie.
     * @return Updated Movie Object.
     */
    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    /**
     * Deletes a Movie Object by its id.
     * @param id of the Movie Object to be deleted.
     */
    @Override
    public void deleteById(Integer id) {
        movieRepository.deleteById(id);
    }

    /**
     * Delete a Movie Object.
     * @param entity of Type Movie.
     */
    @Override
    public void delete(Movie entity) {

    }

    /**
     * Adds a Franchise to a Movie with the specified id. If a Franchise exists on the Movie, it will be replaced.
     * @param movieId of the Movie to add the Franchise to.
     * @param franchises to be added to the Movie.
     */
    @Override
    public void addFranchise(Integer movieId, Franchise franchises) {
        Movie movie = findById(movieId);
        movie.setFranchise(franchises);
        movieRepository.save(movie);
    }

    /**
     * Removes Franchise from movie by setting null.
     * @param movieId of the Movie to remove Franchise from.
     */
    @Override
    public void removeFranchise(Integer movieId) {
        Movie movie = findById(movieId);
        movie.setFranchise(null);
        movieRepository.save(movie);
    }

    /**
     * Removes a Character From a Movie.
     * @param movieId of the Movie to remove Character from.
     * @param characterId of the Character to remove from Movie.
     */
    @Override
    public void removeCharacter(Integer movieId, Integer characterId) {
        Movie movie = findById(movieId);
        movie.getCharacters().removeIf(c -> characterId.equals(c.getId()));
        movieRepository.save(movie);
    }
}
