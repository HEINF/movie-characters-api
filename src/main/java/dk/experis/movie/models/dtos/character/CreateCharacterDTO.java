package dk.experis.movie.models.dtos.character;

import lombok.Data;

@Data
public class CreateCharacterDTO {
    private String fullName;
    private String alias;
    private String gender;
    private String picture;
}
