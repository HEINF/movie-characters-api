package dk.experis.movie.models.dtos.character;

import lombok.Data;

import java.util.Set;

@Data
public class UpdateCharacterDTO {
    private String fullName;
    private String alias;
    private String gender;
    private String picture;
}
