package dk.experis.movie.models.dtos.franchise;

import lombok.Data;

import java.util.Set;

@Data
public class FranchiseDTO {
    private Integer id;
    private String name;
    private String description;
    private Set<Integer> movies;
}
