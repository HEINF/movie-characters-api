package dk.experis.movie.models.dtos.franchise;

import lombok.Data;

import java.util.Set;

@Data
public class UpdateFranchiseDTO {
    private String name;
    private String description;
    private Set<Integer> movies;
}
