package dk.experis.movie.models.dtos.franchise;

import lombok.Data;

@Data
public class CreateFranchiseDTO {
    private String name;
    private String description;
}
