package dk.experis.movie.models.dtos.movie;

import lombok.Data;

import java.util.Set;

@Data
public class MovieDTO {
    private Integer id;
    private String title;
    private String genre;
    private Integer year;
    private String director;
    private String picture;
    private String trailer;
    private Set<Integer> characters;
    private Integer franchise;
}
