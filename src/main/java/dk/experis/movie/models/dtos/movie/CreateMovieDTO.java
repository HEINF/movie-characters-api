package dk.experis.movie.models.dtos.movie;

import lombok.Data;

@Data
public class CreateMovieDTO {
    private String title;
    private String genre;
    private Integer year;
    private String director;
    private String picture;
    private String trailer;
}
