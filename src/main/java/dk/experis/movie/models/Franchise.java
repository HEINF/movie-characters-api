package dk.experis.movie.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {

    @Id
    @Column(name = "franchise_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 50, nullable = false)
    private String name;

    @Column(length = 700)
    private String description;

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}
