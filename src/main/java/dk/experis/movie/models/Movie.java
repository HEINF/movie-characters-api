package dk.experis.movie.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {

    @Id
    @Column(name = "movie_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "title", length = 50, nullable = false)
    private String title;

    @Column( length = 80)
    private String genre;

    @Column(name = "year", length = 4)
    private int year;

    @Column(length = 50)
    private String director;

    @Column(name = "picture_url", length = 200)
    private String picture;

    @Column(name = "trailer_url", length = 200)
    private String trailer;

    @ManyToMany()
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id", nullable = true)}
    )
    private Set<Character> characters;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

}
