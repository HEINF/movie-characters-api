package dk.experis.movie.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private int id;

    @Column(name = "name", length = 50, nullable = false)
    private String fullName;

    @Column(length = 40)
    private String alias;

    @Column(length = 20)
    private String gender;

    @Column(name = "picture_url", length = 200)
    private String picture;

    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;

}
