package dk.experis.movie.mappers;

import dk.experis.movie.models.Franchise;
import dk.experis.movie.models.Movie;
import dk.experis.movie.models.dtos.franchise.CreateFranchiseDTO;
import dk.experis.movie.models.dtos.franchise.FranchiseDTO;
import dk.experis.movie.models.dtos.franchise.UpdateFranchiseDTO;
import dk.experis.movie.services.MovieService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;

    // Auto mapping
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);

    public abstract Collection<FranchiseDTO> franchiseToFranchiseDTO(Collection<Franchise> franchises);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovie")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO dto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target =  "movies", ignore = true)
    public abstract Franchise createFranchiseDtoToFranchise(CreateFranchiseDTO dto);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovie")
    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract Franchise updateFranchiseDtoToFranchise(UpdateFranchiseDTO dto, @MappingTarget Franchise entity);

    // Custom mapping
    @Named("moviesToIds")
    Set<Integer> map(Set<Movie> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Named("movieIdsToMovie")
    Set<Movie> mapIdsToMovies(Set<Integer> id) {
        if(id == null) {
            return null;
        }
        //return id.stream().map(i -> movieService.findById(i)).collect(Collectors.toSet());
        Set<Movie> movies = id.stream().map(i -> movieService.findById(i)).collect(Collectors.toSet());
        return movies;
    }
}
