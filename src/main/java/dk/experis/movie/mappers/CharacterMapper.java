package dk.experis.movie.mappers;

import dk.experis.movie.models.Character;
import dk.experis.movie.models.Movie;
import dk.experis.movie.models.dtos.character.CharacterDTO;
import dk.experis.movie.models.dtos.character.CreateCharacterDTO;
import dk.experis.movie.models.dtos.character.UpdateCharacterDTO;
import dk.experis.movie.services.MovieService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    protected MovieService movieService;

    // Auto mapping
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieToMovieId")
    public abstract CharacterDTO characterToCharacterDTO(Character character);

    public abstract Collection<CharacterDTO> characterToCharacterDTO(Collection<Character> characters);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovie")
    public abstract Character characterDtoToCharacter(CharacterDTO dto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "movies", ignore = true)
    public abstract Character createCharacterDtoToCharacter(CreateCharacterDTO dto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "movies", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract Character updateCharacterDtoToCharacter(UpdateCharacterDTO dto, @MappingTarget Character entity);

    // Custom mappings
    @Named("movieToMovieId")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null) {
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }
    @Named("movieIdToMovie")
    Set<Movie> mapMovieIdsToMovies(Set<Integer> id) {
        if(id == null) {
            return null;
        }
        return id.stream().map(i -> movieService.findById(i)).collect(Collectors.toSet());
    }
}
