package dk.experis.movie.mappers;

import dk.experis.movie.models.Character;
import dk.experis.movie.models.Franchise;
import dk.experis.movie.models.Movie;
import dk.experis.movie.models.dtos.movie.CreateMovieDTO;
import dk.experis.movie.models.dtos.movie.MovieDTO;
import dk.experis.movie.models.dtos.movie.UpdateMovieDTO;
import dk.experis.movie.services.CharacterService;
import dk.experis.movie.services.FranchiseService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected FranchiseService franchiseService;
    @Autowired
    protected CharacterService characterService;

    // Auto mapping
    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDTO(Movie movie);

    public abstract Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movies);

    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdsToCharacters")
    public abstract Movie movieDtoToMovie(MovieDTO dto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "characters", ignore = true)
    @Mapping(target = "franchise", ignore = true)
    public abstract Movie createMovieDtoToMovie(CreateMovieDTO dto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdsToCharacters")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract Movie updateMovieDtoToMovie(UpdateMovieDTO dto, @MappingTarget Movie entity);

    // Custom mapping
    @Named("charactersToIds")
    Set<Integer> mapCharactersToIds(Set<Character> source) {
        if(source == null) {
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Named("characterIdsToCharacters")
    Set<Character> mapIdsToCharacters(Set<Integer> id) {
        if(id == null){
            return null;
        }
        return id.stream().map(i -> characterService.findById(i)).collect(Collectors.toSet());
    }

    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(Integer id) {
        if (id == null) {
            return null;
        }
        return franchiseService.findById(id);
    }
}
