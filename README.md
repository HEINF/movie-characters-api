# Movie Characters API
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

# Purpose
This project is a Spring Boot Web application using JPA and Hibernate in combination with a PostgreSQL Database.

## Prerequisites 
Requires Docker to run.

## Install
Clone the repository to a compatible IDE of choice.

## Usage
A Docker image is available under GitLab Containers, however this image does not include a database.
To run the application with a database, use Docker Compose.

From a terminal in the root of the project type the following commands:
- docker build -t registry.gitlab.com/heinf/movie-characters-api .
- docker-compose up

Then the application will be available on localhost on port 8080.
Swagger documentation is then available locally here: http://localhost:8080/swagger-ui/index.html

## Contributing
Not open for contributions.

## License
Unknown
